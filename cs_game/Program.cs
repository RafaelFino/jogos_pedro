﻿using System;
using cs_game;
using System.Threading;
using System.Threading.Tasks;
using cs_game.source;

namespace home
{
    class Program
    {
        static void Main(string[] args)       
        {
            while(true)
            {
                Start();

                Console.WriteLine("Play again? (Y/N)");
                if(Console.ReadKey(true).Key != ConsoleKey.N)
                {
                    Start();
                } 
                else
                {
                    return;
                }
            }
        }

        static void Start()
        {
            var game = new Game();

            game.Start();

            while(game.Running)
            {
                    switch(Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.Escape:
                        {
                            game.Stop(); 
                            break;
                        }
                        case ConsoleKey.LeftArrow:
                        {
                            game.PlayerLeft();
                            break;
                        }
                        case ConsoleKey.RightArrow:
                        {
                            game.PlayerRight();
                            break;
                        }
                        case ConsoleKey.Spacebar:
                        {
                            game.Shoot();
                            break;
                        }
                    }
            }
        }
    }
}
