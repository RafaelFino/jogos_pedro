using System;

namespace cs_game.source
{
    public class Bomb : Obj
    {
        public Bomb(int H, int W, Move direction, char background)
        {
            Pos.H = H;
            Pos.W = W;
            Shadow = background;

            Name = Guid.NewGuid().ToString();
            Type = ObjType.Bomb;

            Skin.Add(Move.Up, '*');
            Skin.Add(Move.Down, '*');
            Skin.Add(Move.Left, '*');
            Skin.Add(Move.Right, '*');
            Skin.Add(Move.Collide, '*');

            Status = direction;
        }

        public override void OnCollide()
        {
        }
    }
}