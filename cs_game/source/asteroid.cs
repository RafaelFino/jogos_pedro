using System;

namespace cs_game.source
{
    public class Asteroid : Obj
    {
        public Asteroid(int H, int W, Move direction, char endSkin)
        {
            Pos.H = H;
            Pos.W = W;
            Shadow = '.';

            Name = Guid.NewGuid().ToString();
            Type = ObjType.Asteroid;

            Skin.Add(Move.Up, '@');
            Skin.Add(Move.Down, '@');
            Skin.Add(Move.Left, '@');
            Skin.Add(Move.Right, '@');
            Skin.Add(Move.Collide, endSkin);

            Status = direction;
        }

        public override void OnCollide()
        {
        }
    }
}