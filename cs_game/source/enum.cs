namespace cs_game.source
{
    public enum Move
    {
        Stop = 0,
        Up = 1,
        Down = 2,
        Left = 3,
        Right = 4,
        Collide = 5,
    }   

    public enum ObjType
    {
        Player = 0,
        Asteroid = 1,
        Bomb = 2
    }
}

