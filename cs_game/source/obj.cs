using System;
using System.Collections;
using System.Collections.Generic;

namespace cs_game.source
{
    public abstract class Obj
    {
        public string Name;

        public Position Pos = new Position();

        public Move Status;

        public char Shadow = ' ';

        public ObjType Type;

        public Dictionary<Move, char> Skin = new Dictionary<Move, char>();

        public char GetSkin()
        {
            char ret;
            if(!Skin.TryGetValue(Status, out ret))
            {
                ret = '@';
            }

            return ret;
        }

        public abstract void OnCollide();
    }
}