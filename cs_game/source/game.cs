using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;

namespace cs_game.source
{
    public delegate void GameOverHandler();
    public delegate void CollideHandler();

    public class Game
    {
        private Arena _arena;
        private Player _player;
        public bool Running = false;

        private int _interval = 100;

        private int _score = 0;

        public Game()
        {
            _arena = new Arena(56, 220);
            _player = new Player(_arena.SizeH/2, _arena.SizeW/2, _arena.Background, () => GameOver());

            _arena.AddObj(_player);
        }

        public void Start()
        {
            Running = true;

            var show = new Task(() => Show());
            show.Start();

            var asteroids = new Task(() => CreateAsteroids());
            asteroids.Start();
        }

        public void Stop()
        {
            Running = false;
        }

        public void PlayerLeft()
        {
            switch(_player.Status)
            {
                case Move.Up:
                {
                    _arena.Change(_player.Name, Move.Left);
                    break;
                }
                case Move.Left:
                {
                    _arena.Change(_player.Name, Move.Down);
                    break;
                }
                case Move.Down:
                {
                    _arena.Change(_player.Name, Move.Right);
                    break;
                }
                case Move.Right:
                {
                    _arena.Change(_player.Name, Move.Up);
                    break;
                }                                                
            }
        }

        public void PlayerRight()
        {
            switch(_player.Status)
            {
                case Move.Up:
                {
                    _arena.Change(_player.Name, Move.Right);
                    break;
                }
                case Move.Left:
                {
                    _arena.Change(_player.Name, Move.Up);
                    break;
                }
                case Move.Down:
                {
                    _arena.Change(_player.Name, Move.Left);
                    break;
                }
                case Move.Right:
                {
                    _arena.Change(_player.Name, Move.Down);
                    break;
                }                                                
            }            
        }        

        public void Shoot()
        {
            switch(_player.Status)
            {
                case Move.Up:
                {
                    _arena.AddObj(new Bomb(_player.Pos.H-2, _player.Pos.W, _player.Status, _arena.Background));
                    break;
                }
                case Move.Left:
                {
                    _arena.AddObj(new Bomb(_player.Pos.H, _player.Pos.W-2, _player.Status, _arena.Background));
                    break;
                }
                case Move.Down:
                {
                    _arena.AddObj(new Bomb(_player.Pos.H+2, _player.Pos.W, _player.Status, _arena.Background));
                    break;
                }
                case Move.Right:
                {
                    _arena.AddObj(new Bomb(_player.Pos.H, _player.Pos.W+2, _player.Status, _arena.Background));
                    break;
                }                                                
            }  
            
        }

        private void CreateAsteroids()
        {
            Random rdm = new Random();
            var directions = new List<Move> { Move.Up, Move.Down, Move.Right, Move.Left };
            var sleep = 1200;
            
            while(Running)
            {
                _arena.AddObj(new Asteroid(rdm.Next(3, _arena.SizeH-4), rdm.Next(3, _arena.SizeW-4), directions[rdm.Next(0, directions.Count-1)] , _arena.Background));
                Thread.Sleep(sleep);

                if (sleep > 200)
                {
                    sleep -= 200;
                } 
            }
        }

        public void Show()
        {
            Running = true;
            var data = new StringBuilder(_arena.SizeH*_arena.SizeW);
            Console.BackgroundColor = ConsoleColor.Black;
            while(Running)
            {
                foreach(var line in _arena.GetLines())
                {
                    data.AppendLine(line);
                }
                
                Console.Clear();
                Console.SetCursorPosition(0,0);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(data.ToString());
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(">> SCORE: " + (_score + _arena.Score).ToString());
                data.Clear();

                //Console.SetCursorPosition(_arena.Player.Pos.W, _arena.Player.Pos.H);
                //Console.Write(_arena.Player.GetSkin());

                Thread.Sleep(_interval);
                _arena.Go();

                _score++;                
            }
        }        

        public void GameOver()
        {
            Running = false;
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("## GAME OVER ##");
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}