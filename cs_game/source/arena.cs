using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace cs_game.source
{
    public class Arena
    {
        public int SizeH;
        public int SizeW;
        public char Background = ' ';
        public char Border = '#';

        public int Score = 0;
        public IDictionary<string, Obj> _objects = new ConcurrentDictionary<string, Obj>();

        private List<List<char>> _arena;

        public Obj Player;

        public Arena(int H, int W)
        {
            SizeH = H;
            SizeW = W;

            _arena = new List<List<char>>();
            for(int h = 0; h < SizeH; h++)
            {
                var line = new List<char>();

                if(h == 0 || h == (SizeH -1))
                {
                    for(int w = 0; w < SizeW; w++)
                    {
                        line.Add(Border);
                    }
                }
                else
                {
                    line.Add(Border);
                    for(int w = 0; w < (SizeW-2); w++)
                    {
                        line.Add(Background);
                    }
                    line.Add(Border);
                }

                _arena.Add(line);
            }
        }

        public IEnumerable<string> GetLines()
        {
            var ret = new List<string>();

            foreach(var line in _arena)
            {
                ret.Add(String.Concat(line));
            }

            return ret;
        }

        public void AddObj(Obj o)
        {
            _objects.Add(o.Name, o);
        }

        public void Change(string name, Move newStatus)
        {
            _objects[name].Status = newStatus;
        }

        public void Go()
        {
            var toRemove = new List<string>();
            foreach(var item in _objects)
            {
                if(item.Value.Status == Move.Collide)
                {
                    return;
                }

                _arena[item.Value.Pos.H][item.Value.Pos.W] = item.Value.Shadow;

                switch(item.Value.Status)
                {
                    case Move.Up:
                    {      
                        item.Value.Pos.H--;    
                        break;
                    }
                    case Move.Down:
                    {      
                        item.Value.Pos.H++;          
                        break;
                    }
                    case Move.Left:
                    {      
                        item.Value.Pos.W--;     
                        break;
                    }
                    case Move.Right:
                    {      
                        item.Value.Pos.W++;        
                        break;
                    }                                                
                }

                var newPos = _arena[item.Value.Pos.H][item.Value.Pos.W];

                _arena[item.Value.Pos.H][item.Value.Pos.W] = item.Value.GetSkin();
                
                switch(item.Value.Type)
                {
                    case ObjType.Player:
                    {
                        if(newPos != Background)
                        {
                            item.Value.OnCollide();
                        }        

                        Player = item.Value;          
                        break;
                    }
                    case ObjType.Asteroid:
                    {
                        if(newPos != Background)
                        {   
                            toRemove.Add(item.Key);
                            _arena[item.Value.Pos.H][item.Value.Pos.W] = Border;
                        }
                        break;
                    }
                    case ObjType.Bomb:
                    {
                        if(newPos != Background)
                        {   
                            Score += 10;
                        }
                        if(item.Value.Pos.H == 0 || item.Value.Pos.H == SizeH-1
                        || item.Value.Pos.W == 0 || item.Value.Pos.W == SizeW-1)
                        {
                            toRemove.Add(item.Key);
                            _arena[item.Value.Pos.H][item.Value.Pos.W] = Border;
                        }
                        break;
                    }
                }
            }

            foreach(var item in toRemove)
            {
                _objects.Remove(item);
            }
        }
    }
}