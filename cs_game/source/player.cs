using System;
using System.Collections;
using System.Collections.Generic;

namespace cs_game.source
{
    public class Player : Obj
    {
        public GameOverHandler OnGameOver;

        public Player(int H, int W, char shadow, GameOverHandler gameOver)
        {
            Pos.H = H;
            Pos.W = W;
            Shadow = shadow;

            Name = "Player";
            Type = ObjType.Player;

            OnGameOver = delegate() { gameOver(); };
            Skin.Add(Move.Up, '^');
            Skin.Add(Move.Down, 'V');
            Skin.Add(Move.Left, '<');
            Skin.Add(Move.Right, '>');
            Skin.Add(Move.Collide, '#');

            Status = Move.Right;
        }

        public override void OnCollide()
        {
            OnGameOver.Invoke();
        }
    }
}