FROM packages.tools.infra:8443/centos/systemd:7

RUN rpm -Uvh https://packages.microsoft.com/config/rhel/7/packages-microsoft-prod.rpm
RUN yum update -y
RUN yum install -y dotnet-sdk-2.2 curl vim wget
VOLUME [ "/data" ]
